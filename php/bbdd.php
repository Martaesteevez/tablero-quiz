<?php
$mysql_host = "localhost:3306";
$mysql_database = "prueba";
$mysql_user = "root";
$mysql_password = "";

// Conexión a la base de datos
$conexion = mysqli_connect($mysql_host, $mysql_user, $mysql_password);
mysqli_select_db ($conexion, $mysql_database);
// Comprobar la conexión
if (mysqli_connect_errno()) {
    echo "Error al conectar con la base de datos: " . mysqli_connect_error();
    exit();
}

// Obtener el resultado del quiz
$resultado = $_SESSION['cont'];

// Insertar el resultado en la base de datos
$nombre = mysqli_real_escape_string($conexion, $_COOKIE['nombre']);
$insert_query = "INSERT INTO resultado (nombre, resultado) VALUES ('$nombre', '$resultado')";

if (mysqli_query($conexion, $insert_query)) {
    echo "Resultados insertados correctamente";
} else {
    echo "Error al insertar resultados: " . mysqli_error($conexion);
}

// Mostrar los resultados de la tabla 'resultado' en una tabla HTML
$select_query = "SELECT * FROM resultado";
$resultado = mysqli_query($conexion, $select_query);

echo "<table>";
echo "<tr>";
echo "<th>id</th>";
echo "<th>nombre</th>";
echo "<th>resultado</th>";
echo "</tr>";

while ($fila = mysqli_fetch_array($resultado)) {
    echo "<tr>";
    echo "<td>" . $fila['id'] . "</td>";
    echo "<td>" . $fila['nombre'] . "</td>";
    echo "<td>" . $fila['resultado'] . "</td>";
    echo "</tr>";
}

echo "</table>";

// Cerrar la conexión
mysqli_close($conexion);
?>
