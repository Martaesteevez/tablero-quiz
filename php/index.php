<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="bootstrap.css">
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <div class="container-fluid">
          <a class="navbar-brand" href="#">QUIZ PREGUNTAS</a>
          <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarColor02" aria-controls="navbarColor02" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarColor02">
            <ul class="navbar-nav me-auto">
              <li class="nav-item">
                <a class="nav-link active" href="pregunta1.php">Pregunta 1
                  <span class="visually-hidden">(current)</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="pregunta2.php">Pregunta 2</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="pregunta3.php">Pregunta 3</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="pregunta4.php">Pregunta 4</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="pregunta5.php">Pregunta 5</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="pregunta6.php">Pregunta 6</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="pregunta7.php">Pregunta 7</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="pregunta8.php">Pregunta 8</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="pregunta9.php">Pregunta 9</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="pregunta10.php">Pregunta 10</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="resultado.php">resultado</a>
              </li>
            </ul>
            <form class="d-flex">
              <input class="form-control me-sm-2" type="search" placeholder="Search">
              <button class="btn btn-secondary my-2 my-sm-0" type="submit">Search</button>
            </form>
          </div>
        </div>
      </nav>
      <div>
        <div>
          <h1 class="titulo">¡VAMOS A JUGAR! </h1>
          <img class="imagen_principal"src="img/jugar.jpeg" alt="vamos a jugar">
        </div>
      <form action="pregunta1.php" method="get">
        <h3 class="titulo_segundo">Introduce un nombre: </h3>
        <input  class="cuadrado"type="text" name="nombre">
        <input type="submit" value="Enviar">
    </form>
      </div>
    

</body>
</html>